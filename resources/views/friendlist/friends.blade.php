<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>friendlist</title>
</head>
<body>
	<ul>
		@foreach($friends as $friend)
		<li>{{ $friend->name }}</li>
		@endforeach
	</ul>
</body>
</html>