<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login Form</title>
</head>
<body>
	<h1>login form</h1>
	<form action="/login" method="POST">
		@csrf
		<label for="email">Email:</label>
		<input type="text" name="email" id="email">
		<br>
		<label for="password">Password:</label>
		<input type="text" name="password" id="password">
		<br>
		<button type="submit">Login</button>
	</form>
</body>