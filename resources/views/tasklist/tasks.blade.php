<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>tasklist</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>


	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Task</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($tasks as $task)
						<tr>
							<td><form action="/task/{{$task->_id}}" method="POST">
										{{ csrf_field() }}
										{{ method_field("PATCH") }}
								<input type="text" class="form-control" name="edittask" id="edittask" placeholder="{{ $task->name }}
								">
								<button type="submit" class="btn btn-primary">Edit Task</button>
							</form>
							</td>
							<td>{{ $task->status }}</td>
							<td>
								<form method="POST" action="/task/{{$task->_id}}">
									{{ csrf_field() }}
									{{ method_field("DELETE") }}
									<button type="submit" class="btn btn-danger">Remove</button>
								</form>
								

										
							
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<form action="task/add" method="POST">
					@csrf
					<input type="text" class="form-control" name="name" id="name">
					<button type="submit" class="btn btn-primary">Add Task</button>
				</form>

			</div>
		</div>
	</body>
	</html>