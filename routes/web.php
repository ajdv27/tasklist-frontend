<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/sample', "SampleController@getContent");
Route::get('/sample2', "SampleController@getOtherContent");


//friends
Route::get('/friend/showAll', "FriendController@showAll");
Route::get('/friend/add', "FriendController@addFriend");
Route::get('/friend/edit/{id}', "FriendController@editFriend");
Route::get('/friend/{id}', "FriendController@showOne");
Route::get('/unfriend/{id}', "FriendController@unfriend");


//tasks
Route::get('/tasklist', "TaskController@showTasks");
Route::post('/task/add', "TaskController@addTask");
Route::delete('/task/{id}', "TaskController@deleteTask");
Route::patch('/task/{id}', "TaskController@editTask");


//admin
Route::get('/register', "AdminController@showRegisterForm");
Route::post('/register', "AdminController@registerUser");
Route::get('/login', "AdminController@showLoginForm");
Route::post('/login', "AdminController@loginUser");
Route::get('/logout', "AdminController@logout");