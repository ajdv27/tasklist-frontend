<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class SampleController extends Controller
{
	public function getContent(){
		$client = new Client(["base_uri" => "http://localhost:3000/"]);
    	//specifies that the frontend connects to teh base URI

    	$response = $client->request("GET", "sample/");
    	//this sends a request to get the / (root) route from the sample in server.js

    	//the response would be received and we a need to process the data
    	dd(json_decode($response->getBody()));
    }

    public function getOtherContent(){
    	$client = new Client (["base_uri" => "http://localhost:3000/"]);
    	$response = $client->request("GET", "sample/greet/");

    	dd(json_decode($response->getBody()));
    }
}
