<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;

class TaskController extends Controller
{
    public function showTasks(){
    	$client = new Client (["base_uri" => "http://localhost:3000/"]);
    	$response = $client->request("GET", "task/", [
            "headers" => [
                "Authorization" => Session::get("token")

            ]
        ]);
    	$tasks = json_decode($response->getBody())->task;
    	return view("tasklist.tasks", compact("tasks"));
    }

    public function addTask(Request $request){
    	$client = new Client (["base_uri" => "http://localhost:3000/"]);
    	$response = $client->request("POST", "task/add", [
            "headers" => [
                "Authorization" => Session::get("token")
            ],

            "json" => [
               "name" => $request->name,
               "status" => "pending"
           ]
       ]);
    	return redirect("/tasklist");
    }

    public function deleteTask($id){
    	$client = new Client (["base_uri" => "http://localhost:3000/"]);
    	$response = $client->request("DELETE", "task/$id", [
            "headers" => [
                "Authorization" => Session::get("token")

            ]
        ]);
    	return redirect("/tasklist");
    }

    public function editTask(Request $request, $id){
      $client = new Client (["base_uri" => "http://localhost:3000/"]);
      $response = $client->request("PUT", "task/$id", [
        "headers" => [
            "Authorization" => Session::get("token")
        ],

        "json" => [
           "name" => $request->edittask,
           "status" => "pending"
       ]

   ]);

      return redirect("/tasklist");
  }

}
