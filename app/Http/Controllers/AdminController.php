<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;

class AdminController extends Controller
{
	public function showRegisterForm(){
		return view("admin.register_form");
	}

	public function registerUser(Request $request){
		$client = new Client (["base_uri" => "http://localhost:3000/"]);
		$response = $client->request("POST", "member/register", [
			"json" => [
				"email" => $request->email,
				"password" => $request->password
			]
		]);
		$result = json_decode($response->getBody());
		dd($result);

	}

	public function showLoginForm(){
		return view("admin.login_form");
	}

	public function loginUser(Request $request){
		$client = new Client (["base_uri" => "http://localhost:3000/"]);
		$response = $client->request("POST", "member/login", [
			"json" => [
				"email" => $request->email,
				"password" => $request->password
			]
		]);
		$result = json_decode($response->getBody());
		//dd($result);
		Session::put("user", $result->data->user);
		Session::put("token", "Bearer " . $result->data->token);

		return redirect("/friend/showAll");
	}

	public function logout(){
		Session::flush();
		return redirect("/login");
	}

}
