<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;

class FriendController extends Controller
{
	public function showAll(){
		$client = new Client (["base_uri" => "http://localhost:3000/"]);
		//$response = $client->request("GET", "friend/");
		$response = $client->request("GET", "friend/", [
			"headers" => [
				"Authorization" => Session::get("token")

			]

		]);
		$friends = json_decode($response->getBody())->friends;
		return view("friendlist.friends", compact("friends"));
	}



	public function showOne($id){
		$client = new Client (["base_uri" => "http://localhost:3000/"]);
		$response = $client->request("GET", "friend/$id");

		dd(json_decode($response->getBody()));
	}

	public function addFriend(){
		$client = new Client (["base_uri" => "http://localhost:3000/"]);
		$response = $client->request("POST", "friend/add", ["json" => [
			"name" => "Jobert",
			"age" => 24,
			"gender" => "male",
			"description" => "a typical friend",
			"status" => "married"
		]
	]);

		dd(json_decode($response->getBody()));
	}

	public function unfriend($id){
		$client = new Client (["base_uri" => "http://localhost:3000/"]);
		$response = $client->request("DELETE", "friend/$id");

		dd(json_decode($response->getBody()));
	}

	public function editFriend($id){
		$client = new Client (["base_uri" => "http://localhost:3000/"]);
		$response = $client->request("PUT", "friend/$id", ["json" => [
			"name" => "Monika",
			"age" => 18,
			"gender" => "female",
			"description" => "a typical girl space friend",
			"status" => "single"
		]

	]);

		dd(json_decode($response->getBody()));
	}

}
